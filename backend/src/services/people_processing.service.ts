import people_data from '../data/people_data.json';

export class PeopleProcessing {
    getById(id: number) {
        return people_data.find((p) => p.id === id);
    }

    getAll() {
        return people_data;
    }

    getUsersByGender(gender: string) {
        return people_data.filter(person => person.gender === gender)
    }
}
