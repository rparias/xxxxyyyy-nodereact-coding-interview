import React from 'react'

interface Props {
  onSubmit: (data: {type?: string, term?: string}) => void
}

const UserForm: React.FC<Props> = ({onSubmit}) => {

  const termInputReference = React.useRef<HTMLInputElement | null>(null)
  const typeSelectReference = React.useRef<HTMLSelectElement | null>(null)

  const formHandler = React.useCallback(
    () => (event: React.FormEvent<HTMLFormElement>) => {
      event.preventDefault()
      const data = {
        type: typeSelectReference.current?.value,
        term: termInputReference.current?.value,
      }
      onSubmit(data)
    }, [onSubmit])

  return (
    <form onSubmit={formHandler()}>
      <div>
        <select name="type" id="type-input" ref={typeSelectReference}>
          <option value="title">Title</option>
          <option value="gender">Gender</option>
          <option value="company">Company</option>
        </select>
        <input 
          type="text"
          name="term"
          ref={termInputReference}
          placeholder="Type a term..."
        />
        <button type='submit'>Submit</button>
      </div>
    </form>
  )
}

export default UserForm