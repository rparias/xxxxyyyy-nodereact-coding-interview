import React, { FC, useState, useEffect } from "react";

import { RouteComponentProps } from "@reach/router";
import { IUserProps } from "../dtos/user.dto";
import { UserCard } from "../components/users/user-card";
import { CircularProgress } from "@mui/material";
import UserForm from "../components/users/user-form";

import { BackendClient } from "../clients/backend.client";

const backendClient = new BackendClient();

export const DashboardPage: FC<RouteComponentProps> = () => {
  const [users, setUsers] = useState<IUserProps[]>([]); // we should useReducer
  const [loading, setLoading] = useState(true); // I'd like to create an object with Users, loading, errors, status, for that, i'd use useReducer to handle that

  useEffect(() => {
    const fetchData = async () => {
      const result = await backendClient.getAllUsers();
      setUsers(result.data.slice(0, 20)); // for now this should be our first page
      setLoading(false)
    };

    fetchData();
  }, []);


  

  const handleOnSubmit = async (data: {type?: string, term?: string}) => {
    if(data.type === 'gender' && data.term !== '') {
      setLoading(true)
      const result = await backendClient.getUsersByGender(data.term!)
      setUsers(result.data.slice(0, 20))
      setLoading(false)
    }
  }

  return (
    <div style={{ paddingTop: "30px" }}>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <UserForm onSubmit={handleOnSubmit} />
        {loading ? (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100vh",
            }}
          >
            <CircularProgress size="60px" />
          </div>
        ) : (
          <div>
            {users.length
              ? users.map((user) => {
                  return <UserCard key={user.id} {...user} />;
                })
              : null}
          </div>
        )}
      </div>
    </div>
  );
};
